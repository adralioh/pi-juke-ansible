# pi-juke-ansible
Ansible role for configuring a Raspberry Pi MPD server from a base install

Designed for Raspberry Pi OS Buster running on a Pi 3 Model B

## Feature Overview
Many of the options mentioned in this section are configurable, even if not mentioned\
See [Variables](#variables) for more information

### MPD
Two instances of mpd are configured: `mpd@local` and `mpd@stream`\
`mpd@local` outputs to a local audio device (defaults to the headphone jack)\
`mpd@stream` outputs to an HTTP stream accessible at *http://HOSTNAME:8080*

The mpd instances are managed on ports 6600 and 6601 respectively via an mpd client such as mpc\
Both instances are password protected. The passwords are configured via variables

Each mpd instance uses [ashuffle](https://github.com/joshkunz/ashuffle) to add a random song to the queue when it hits the end\
This allows for endless playback\
To prevent the mpd queue from getting too big due to this, there is a daily timer for each instance called `mpc-crop@INSTANCE.timer` that removes all songs in the queue except for the currently playing one

By default, the `mpd@local` instance starts in a stopped state with one random song queued, and the `mpd@stream` instance starts playing immediately\
The random song is added via the `mpc-random@local` service\
Adding a song to the queue jumpstarts `ashuffle` so that you don't have to manually add a song to the playlist before it will start playing\
If you want the `mpd@local` instance to start playing immediately instead, remove `--tweak play-on-startup=no` from `pi_juke_ashuffle_local_args` and set `pi_juke_mpd_local_add_random` to *False*

There is a third mpd instance called `mpd-database` that manages a shared music database that the other two instances access\
This stops there from being multiple redundant databases, and prevents the two mpd instances from going out of sync when the music library is changed\
The database instance doesn't have an audio output, and is not accessible externally\
You can update the database by running `mpc update` against either of the other two instances

All three instances have a socket listening in `/run/mpd`\
This can be used to control them locally without providing a password\
The global env var `MPD_HOST` is set to the socket for `mpd@local`, so you can access `mpd@local` via `mpc` without any additional arguments

`mpd@local` and `mpd@stream` are managed by the `mpd@local.service` and `mpd@stream.service` services respectively\
Starting/stopping the services will caused all other related units such as `ashuffle` and the music mount to start and/or stop as well

The `mpd-controller@local` service allows you to control the `mpd@local` instance via a USB numpad/keyboard\
Hotkeys are configured via the `pi_juke_controller_commands` var\
If you're not sure what keycode to use, you can run `journalctl -fu mpd-controller@local` to see a log of what keys are being pressed\
If nothing shows up in the logs or the service isn't running, you probably need to change the device that's being monitored via the `pi_juke_controller_device` var\
This unit supports USB hotplug via the `mpd-controller@local.path` unit, which automatically starts the service when the device is connected

### Music Library
The music library is a remote fileshare mounted at `/music`

NFS+Kerberos and SFTP are supported. The type used is set via the `pi_juke_mount_fs_type` var

The fileshare must be configured separately

When using an NFS share, there is a daily timer called `mpd-kinit.timer` that automatically renews the mpd user's ticket via a keytab file

### Root FS Overlay
If the `pi_juke_overlay` variable is set to *True* (default), a tmpfs overlay will be mounted over the rootfs

This means that all file changes will only be written to memory, and will be lost when the Pi is rebooted

This prevents wear on the SD card, and prevents file corruption if the Pi loses power

The `/boot` partition is also mounted read-only

If you need to make changes on disk, you can run `remount-rw`, which will remount the filesystem rw at `/ro`

The downside of this is that mpd's music database and state will be reset as well\
You can avoid the music database being reset by moving it to the music share. This is configured via the `pi_juke_mpd_database` var\
You'll also need to change the boot options so that the share is mounted rw via the `pi_juke_nfs_mount_options` or `pi_juke_sftp_mount_options` var

The script used to set up the root overlay was taken from [here](https://wiki.psuter.ch/doku.php?id=solve_raspbian_sd_card_corruption_issues_with_read-only_mounted_root_partition)

systemd recently gained support for mounting a volatile root overlay in version 242, but Debian Buster is running version 241, so it isn't available yet\
It might be worth switching to this method in a later version\
See [this page](https://www.enricozini.org/blog/2019/himblick/read-only-rootfs/) for more info about what's holding it back

### Automatic Updates
If the rootfs overlay is enabled, a weekly timer called `pi-update.timer` will update and reboot the Pi automatically

You can also run `pi-update` to update and reboot manually

The script works by running `remount-rw` and chrooting into the rw root filesystem

## Variables
Default variables are defined in *roles/pi_juke/defaults/main.yml*\
You can override the default variables by redefining them in *roles/pi_juke/vars/main.yml*

Some of the variables have placeholder default values, so be sure to review them before running the playbook

## Prerequisites
1.  Download the lite image for Raspberry Pi OS from <https://www.raspberrypi.org/downloads/raspberry-pi-os/>
1.  If installing on a headless Pi, you'll need to enable SSH by modifying the image file

    SSH is enabled by creating a file called *ssh* at the root of the boot partition

    The following will mount an image file called *raspios-lite.img* and create the *ssh* file
    ```shell
    loop=$(losetup -f)
    losetup -P "$loop" raspios-lite.img
    mount "${loop}p1" /mnt

    touch /mnt/ssh

    umount /mnt
    losetup -d "$loop"
    ```
    If the Pi isn't headless, you can skip this step and enable the SSH service manually instead

1.  Download ashuffle and copy the binary to *files/ashuffle*

    Source and pre-built binaries are available at <https://github.com/joshkunz/ashuffle>

1.  For NFS music shares

    1.  Copy the host's kerberos keytab to *files/host.keytab*

        This will be used to mount the nfs share

        If using FreeIPA/IdM, you can get the keytab by using `ipa-getkeytab`
        ```shell
        ipa-getkeytab -p "host/$HOSTNAME" -k host.keytab
        ```
    1.  Copy the nfs user's kerberos keytab to *files/user.keytab*

        If using FreeIPA/IdM, you can get the keytab by using `ipa-getkeytab`
        ```shell
        ipa-getkeytab -p "$USERNAME" -k user.keytab
        ```

1.  For SFTP music shares

    Copy the SSH private and public keys to *files/music_key* and *files/music_key.pub* respectively

1.  Write the image to the Pi's SD card and start the Pi

    Changing the password of the *pi* user and/or configuring SSH keys is also recommended

You should be able to run the playbook now

## Running
The following example will run the playbook against the host *HOSTNAME* and prompt for the SSH password
```shell
ansible-playbook -ki "$HOSTNAME", pi-juke.yml
```
A reboot is required after the playbook has successfully run

If the rootfs overlay is enabled, subsequent runs of this playbook will not persist\
Additionally, since the playbook was designed with the rootfs overlay in mind, there are no handlers for restarting services, so you'll need to either reboot or manually restart affected services on subsequent runs if running without the overlay
