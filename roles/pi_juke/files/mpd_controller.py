#!/usr/bin/env python3
import argparse
import json
import shlex
import subprocess
from typing import Mapping, Sequence

# python-evdev
import evdev


class MacroBoard:
    """Runs commands when keys on the given device are pressed

    :param device_path:
        Path to the input device to monitor

        :Example:
            `/dev/input/event0` or
            `/dev/input/by-id/usb-13ba_0001-event-kbd`

    :param commands:
        Dict of commands to run and their corresponding keycode

        This example will run ``mpc toggle`` when keypad-enter
        (keycode `96`) is pressed, and will run ``mpc next`` when
        keypad-dot (keycode `83`) is pressed::

            commands = {
                # KEY_KPENTER
                96: ('/usr/bin/mpc', 'toggle'),
                # KEY_KPDOT
                83: ('/usr/bin/mpc', 'next')
            }

        If you're not sure what keycodes to use, :func:`start` will
        print the keycode of every keypress it detects

        You can pass an empty dict if you only want to monitor
        keypresses without running commands

    :Example:

    >>> # `commands` should be defined like in the above example
    >>> numpad = MacroBoard('/dev/input/event0', commands)
    >>> numpad.start()
    """
    def start(self) -> None:
        """Start monitoring for keypresses until the process is
        killed
        """
        for event in self._device.read_loop():
            if event.type == evdev.ecodes.EV_KEY and event.value == 1:
                print(f'Read keypress: {event.code}')
                if event.code in self._commands:
                    command = self._commands[event.code]
                    print(f'Running command: {command}')
                    subprocess.Popen(command, stdout=subprocess.DEVNULL)

    def __init__(
        self, device_path: str, commands: Mapping[int, Sequence[str]]
    ):
        self._device = evdev.InputDevice(device_path)

        self._commands = {}
        for keycode, command in commands.items():
            self._commands[int(keycode)] = tuple(command)


def main() -> None:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Runs commands when keys on the given device are pressed',
        epilog='multiple commands and/or json files can be given\n'
               '\n'
               'example:\n'
               '  %(prog)s /dev/input/event0 -c "96=/usr/bin/mpc toggle"'
               ' -c "83=/usr/bin/mpc next"\n'
               '  %(prog)s /dev/input/by-id/usb-13ba_0001-event-kbd'
               ' -f commands.json\n'
               '\n'
               'json file:\n'
               '  the json file should be in the following format:\n'
               '    { "KEYCODE": "COMMAND" }\n'
               '\n'
               '  example:\n'
               '    {\n'
               '      "83": "/usr/bin/mpc next",\n'
               '      "96": "/usr/bin/mpc toggle"\n'
               '    }'
    )
    parser.add_argument(
        'device_path', help='path to the input device to monitor'
    )
    parser.add_argument(
        '-c', '--command', action='append',
        help=(
            'command to run with its corresponding keycode in the format'
            ' "$KEYCODE=$COMMAND"'
        )
    )
    parser.add_argument(
        '-f', '--file', action='append', type=argparse.FileType('r'),
        metavar='JSON', help='path to a json file to read commands from'
    )

    args = parser.parse_args()

    commands = {}

    if args.command:
        for arg in args.command:
            print(f'Parsing command arg: {arg}')
            keycode, command = arg.split('=', 1)
            keycode = int(keycode)
            if keycode in commands:
                parser.error(f'argument command: duplicate keycode: {keycode}')
            commands[keycode] = shlex.split(command)
    if args.file:
        for file in args.file:
            print(f'Parsing json file: {file.name}')
            json_dict = json.load(file)
            file.close()
            for keycode, command in json_dict.items():
                keycode = int(keycode)
                if keycode in commands:
                    parser.error(
                        f'argument file: duplicate keycode: {keycode}'
                    )
                commands[keycode] = shlex.split(command)

    numpad = MacroBoard(args.device_path, commands)
    numpad.start()


if __name__ == '__main__':
    main()
